# TheDiff

## Instructions of use

```
const diff = require('thediff');


let first = '<div class="container"><h1>Title</h1><p>More information <a href="#">Read more</a><p></div>';
let second = '<div class="container"><h1 class="title">Title</h1><p class="headline">To read more about this topic<a href="#">follow this link</a><p></div>';

console.log(diff(first, second));

```
